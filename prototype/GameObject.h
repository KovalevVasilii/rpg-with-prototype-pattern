#pragma once
#include <vector>

using Location = std::pair<int, int>;
class GameObject
{
private:
	Location location = Location(0, 0);
	char sprite = '0';
public:
	virtual bool isLive() = 0;
	virtual void setLiveFalse() = 0;
	char getSprite() const { return sprite; }
	void setSprite(char sprite) { this->sprite = sprite; }
	Location getLocation() { return location; }
	void setLocation(Location location) { this->location = location; }
	GameObject();
	virtual ~GameObject();
};

