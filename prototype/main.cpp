#include "game.h"

Swordsman Swordsman::prototype = Swordsman();
Archer Archer::prototype = Archer();
Knigth Knigth::prototype = Knigth();
int main()
{
	Game* game = new Game();
	std::string answer;
	do
	{
		game->gameCircle();
		std::cout << "Would you like to play again? Y or N?\n";
		std::cin >> answer;
		if (answer != "Y")
		{
			break;
		}
		game->reset();
		game = new Game();
	} while (true);
	std::cin.get();
}