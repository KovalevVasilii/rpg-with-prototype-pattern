#include "hero.h"
Hero::Hero()
{
	money = 5;
	GameObject::setLocation(std::pair<int,int>(0,0));
	setSprite('h');
	army.push_back((Warrior::createWarrior(CombatArm::KnigthProt)));
}

int Hero::getStrenght()
{
	int power = 0;
	for(auto it : army)
	{
		power += it->getStrenght();
	}
	return power;
}