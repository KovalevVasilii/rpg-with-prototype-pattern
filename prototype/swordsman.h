#pragma once
#include "warrior.h"
using namespace std;

class Swordsman : public Warrior
{
public:
	Warrior * clone() {
		return new Swordsman(*this);
	}
	std::string info() { return "Swordsman"; }
private:
	Swordsman() 
	{
		Warrior::addPrototype(CombatArm::SwordsmanProt, this);
		strenght = 2;
	}
	static Swordsman prototype;
};

