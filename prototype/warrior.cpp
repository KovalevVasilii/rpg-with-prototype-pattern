#include "warrior.h"
using Registry = std::map<CombatArm, Warrior*>;
Registry& getRegistry()
{
	static Registry _instance;
	return _instance;
}
Warrior* Warrior::createWarrior(CombatArm id)
{
	Registry& registry = getRegistry();
	if (registry.find(id) != registry.end())
		return registry[id]->clone();
	return 0;
}
void Warrior::addPrototype(CombatArm id, Warrior * prototype) {
	Registry& registry = getRegistry();
	registry[id] = prototype;
}
// �������� ��������� �� ��������� ����������
void Warrior::removePrototype(CombatArm id) {
	Registry& registry = getRegistry();
	registry.erase(registry.find(id));
}
