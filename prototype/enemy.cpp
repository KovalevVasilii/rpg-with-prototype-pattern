#include "enemy.h"
#include <cmath>

void Enemy::findVictim(Hero& hero)  
{
	Location heroLocation = hero.getLocation();
	Location myLocation = this->getLocation();
	if (getRad(myLocation.first, myLocation.second,
		heroLocation.first, heroLocation.second) <= radOfView)
	{
		if (myLocation.first > heroLocation.first)
		{
			myLocation.first -= 1 * speed;
			setLocation(myLocation);
			return;
		}
		else if(myLocation.first < heroLocation.first)
		{
			myLocation.first += 1 * speed;
			setLocation(myLocation);
			return;
		}
		else if (myLocation.second > heroLocation.second)
		{
			myLocation.second -= 1 * speed;
			setLocation(myLocation);
			return;
		}
		else if (myLocation.second < heroLocation.second)
		{
			myLocation.second += 1 * speed;
			setLocation(myLocation);
		}
	}
}

int Enemy::getRad(int xa, int ya, int xb, int yb)
{
	return sqrt((xb - xa)*(xb - xa) + (yb - ya)*(yb - ya));
}
Enemy::Enemy()
{
	setSprite('e');
	radOfView = 2;
}


Enemy::~Enemy()
{
}
