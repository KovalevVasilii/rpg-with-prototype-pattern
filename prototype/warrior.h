#pragma once

#include "common.h"
#include <iostream>
#include <map>

class Warrior
{
public:
	Warrior() = default;
	virtual Warrior* clone() = 0;
	virtual ~Warrior() = default;
	virtual std::string info() = 0;
	int getStrenght() { return strenght; }
	static Warrior* createWarrior(CombatArm id);
protected:
	int strenght = 0;
	static void addPrototype(CombatArm id, Warrior * prototype);
	static void removePrototype(CombatArm id);	
};

