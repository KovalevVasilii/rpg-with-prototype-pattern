#pragma once
#include <list>
#include <iostream>
#include <vector>
#include <map>
#include <list>
#include <string>

#include "archer.h"
#include "knigth.h"
#include "swordsman.h"
#include "hero.h"
#include "GameObject.h"
#include "tavern.h"
#include "enemy.h"

class Game
{
private:
	bool checkWin();
	void lose();
	bool checkBorder(Location& location);
	void fisrtSettings();
	void printMap();
	void finalize();
	void readMove();
	void readEvents();
	void battle(GameObject& hero, GameObject& enemy);
	void visitTavern();
	std::list<GameObject*> gameObjects;
	char Map[map_size][map_size];
	GameObject* hero;
	GameObject* enemy;
	GameObject* enemy2;
	GameObject* tavern;
public:
	void gameCircle();
	void reset();
	Game();
	~Game();
};

