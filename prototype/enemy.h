#pragma once
#include "hero.h"
class Enemy :
	public Hero
{
private:
	int radOfView;
	int speed = 1;
	int getRad(int xa, int ya, int xb, int yb);
public:
	void findVictim(Hero& hero);
	void setSpeed(int speed) { this->speed = speed; }
	int getRadOfView() { return radOfView; }
	void setRadOfView(int radOfView) { this->radOfView = radOfView; }
	Enemy();
	~Enemy();
};

