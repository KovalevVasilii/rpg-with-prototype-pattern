#pragma once
#include "warrior.h"

using namespace std;

class Archer : public Warrior
{
public:
	Warrior * clone() {return new Archer(*this);}
	std::string info() { return "Archer"; }
private:
	Archer() { addPrototype(CombatArm::ArcherProt, this); strenght = 1; }
	static Archer prototype;
};
