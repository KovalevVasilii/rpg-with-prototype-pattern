#pragma once

#include <vector>

#include "GameObject.h"
#include "warrior.h"

class Hero : public GameObject
{
private:
	std::vector<Warrior*> army;

public:
	int money = 0;
	int getStrenght();
	bool isLive() { return !army.empty(); }
	void setLiveFalse() { army.clear(); }
	void addWarrior(Warrior& warrior) { army.push_back(&warrior); }
	Hero(std::vector<Warrior*>& army) : army(army){}
	Hero();
	~Hero() = default;
	friend std::ostream& operator<< (std::ostream &out, Hero& hero);
};

inline std::ostream& operator<< (std::ostream &out, Hero& hero)
{
	out << "Power= " << hero.getStrenght() << ", Army: \n";
	for (auto it : hero.army)
	{
		auto str = it->info();
		out << str.c_str();
		out << "\n";
	}
	return out;
}

