#pragma once
#include "warrior.h"
using namespace std;
class Knigth :
	public Warrior
{
public:
	Warrior * clone() {
		return new Knigth(*this);
	}
	std::string info() { return "Knigth"; }
private:
	Knigth() {
		addPrototype(CombatArm::KnigthProt, this);
		strenght = 5;
	}
	static Knigth prototype;
};

