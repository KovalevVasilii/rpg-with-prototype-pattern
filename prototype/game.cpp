#include "game.h"

Game::Game()
{
	hero = new Hero;
	enemy = new Enemy;
	enemy2 = new Enemy;
	tavern = new Tavern;
	Game::fisrtSettings();
}

void Game::reset()
{
	hero = new Hero;
	tavern = new Tavern;
	Game::fisrtSettings();
	printMap();
}
bool Game::checkWin()
{
	if (gameObjects.size() == 2)
	{
		system("cls");
		std::cout << "WIN!!!\n You killed all enemies, my lord!\n";
		std::cout << "I didn't think what you will finish my game. You are awesome!\n";
		std::cout << *(reinterpret_cast<Hero*>(hero)) << std::endl;
		return true;
	}
	return false;
}
Enemy* createEnemy(int x, int y, int speed, int radOfView)
{
	Enemy* enemy = new Enemy();
	enemy->setLocation(std::pair<int, int>(x, y));
	enemy->setRadOfView(radOfView);
	enemy->setSpeed(speed);
	return enemy;
}

Enemy* createStromgEnemy(int x, int y, int speed, int radOfView)
{
	Enemy* enemy = createEnemy(x, y, speed, radOfView);
	enemy->addWarrior(*Warrior::createWarrior(CombatArm::KnigthProt));
	return enemy;
}

bool Game::checkBorder(Location& location)
{
	if (location.first >= 0 && location.first < map_size) {
		if (location.second >= 0 && location.second < map_size - 1) {
			return true;
		}
	}
	return false;
}

void Game::fisrtSettings()
{
	std::memset(&Map, ' ', sizeof(Map));
	hero->setLocation(std::pair<int, int>(1, 1));
	enemy = createEnemy(2, 1, 1, 3);
	enemy2 = createStromgEnemy(4, 5, 1, 2);
	tavern->setLocation(std::pair<int, int>(7, 8));
	tavern->setSprite('t');
	reinterpret_cast<Hero*>(enemy2)->addWarrior(*Warrior::createWarrior(CombatArm::KnigthProt));
	gameObjects.push_back(tavern);
	gameObjects.push_back(hero);
	gameObjects.push_back(enemy);
	gameObjects.push_back(enemy2);
}

void Game::printMap()
{
	std::memset(&Map, ' ', sizeof(Map));
	for (auto it : gameObjects)
	{
		Location location = it->getLocation();
		Map[location.first][location.second + 1] = it->getSprite();
	}
	for (int i = 0; i < map_size + 2; i++)
	{
		std::cout << border;
	}
	std::cout << "\n";
	for (int i = 0; i < map_size; i++)
	{
		std::cout << border;
		for (int j = 0; j < map_size; j++)
		{
			std::cout << Map[i][j];
		}
		std::cout << border << "\n";
	}
	for (int i = 0; i < map_size + 2; i++)
	{
		std::cout << border;
	}
}

void Game::finalize()
{
	for (auto it : gameObjects)
	{
		it->~GameObject();
	}
}

void Game::readMove()
{
	std::string answer;
	std::cout << "\nWhat will be the orders, my lord?\n";
	std::cin >> answer;
	Location location = hero->getLocation();

	if (answer.size() != 0) {
		if (answer == "w") {
			location.first -= 1;
			if (checkBorder(location)) {
				hero->setLocation(location);
			}
		}
		if (answer == "s") {
			location.first += 1;
			if (checkBorder(location)) {
				hero->setLocation(location);
			}
		}
		if (answer == "d") {
			location.second += 1;
			if (checkBorder(location)) {
				hero->setLocation(location);
			}
		}
		if (answer == "a") {
			location.second -= 1;
			if (checkBorder(location)) {
				hero->setLocation(location);
			}
		}
		if (answer == "exit") {
			hero->setLiveFalse();
		}
	}
}

void Game::battle(GameObject& hero, GameObject& enemy)
{
	system("cls");
	std::cout << "Battle!\n";
	std::cout << "Hero: " << *(reinterpret_cast<Hero*>(&hero));
	std::cout << "Enemy: " << *reinterpret_cast<Hero*>(&enemy);
	if ((*(reinterpret_cast<Hero*>(&hero))).getStrenght() >= (*(reinterpret_cast<Hero*>(&enemy))).getStrenght())
	{
		std::cout << "WIN!";
		enemy.setLiveFalse();
		reinterpret_cast<Hero*>(this->hero)->money += 5;
	}
	else
	{
		std::cout << "LOSE!";
		hero.setLiveFalse();
	}
	std::cin.get();
	std::cin.get();
}

void Game::visitTavern()
{
	Hero& hero = *(reinterpret_cast<Hero*>(this->hero));
	system("cls");
	std::cout << "Tavern!\n";
	std::cout << "Hero: " << hero;
	std::cout << "\nMoney: " << hero.money << std::endl;
	std::cout << "1. Buy archer: 2 coins\n";
	std::cout << "2. Buy swordsman: 5 coins\n";
	std::cout << "3. Buy knigth: 10 coins\n";
	std::cout << "4. Exit\n";
	std::string answer;
	std::cin >> answer;
	while (true) {

		if (answer.size() != 0)
		{
			if (answer == "1")
			{
				if (hero.money - 2 >= 0)
				{
					hero.money -= 2;
					hero.addWarrior((*Warrior::createWarrior(CombatArm::ArcherProt)));
				}
				else
				{
					std::cout << "You need more money!\n";
					std::cin.get();
					std::cin.get();
				}

			}
			else if (answer == "2")
			{
				if (hero.money - 5 >= 0)
				{
					hero.money -= 5;
					hero.addWarrior((*Warrior::createWarrior(CombatArm::SwordsmanProt)));
				}
				else
				{
					std::cout << "You need more money!\n";
					std::cin.get();
					std::cin.get();
				}
			}
			else if (answer == "3")
			{
				if (hero.money - 10 >= 0)
				{
					hero.money -= 10;
					hero.addWarrior((*Warrior::createWarrior(CombatArm::KnigthProt)));
				}
				else
				{
					std::cout << "You need more money!\n";
					std::cin.get();
					std::cin.get();
				}
			}
			else if (answer == "4")
			{
				break;
			}
		}
		system("cls");
		std::cout << "Tavern!\n";
		std::cout << "Hero: " << hero;
		std::cout << "\nMoney: " << hero.money << std::endl;
		std::cout << "1. Buy archer: 2 coins\n";
		std::cout << "2. Buy swordsman: 5 coins\n";
		std::cout << "3. Buy knigth: 10 coins\n";
		std::cout << "4. Exit\n";
		std::cin >> answer;
	}
	std::cin.get();
	std::cin.get();
}
void Game::readEvents()
{
	for (auto it : gameObjects)
	{
		if (it->getSprite() == 'e')
		{
			Enemy& en = *(reinterpret_cast<Enemy*>(it));
			Hero& hero = *(reinterpret_cast<Hero*>(this->hero));
			en.findVictim(hero);
		}
	}
	for (auto it : gameObjects)
	{
		if (it->getLocation() == hero->getLocation())
		{
			if (it->getSprite() == 't') {
				visitTavern();
			}
			else if(hero != it)
			{
					battle(*hero, *it);
					break;
			}
		}
	}
	for (auto it : gameObjects)
	{
		if (!it->isLive())
		{
			gameObjects.remove(it);
			return;
		}
	}
}
void Game::lose()
{
	system("cls");
	std::cout << "Lose...\n My lord... My lord...\n";
	std::cout << *(reinterpret_cast<Hero*>(hero)) << std::endl;
	std::cin.get();
	std::cin.get();
}
void Game::gameCircle()
{
	bool gameLife = true;

	while (gameLife)
	{
		system("cls");
		std::cout << *(reinterpret_cast<Hero*>(this->hero)) << std::endl;
		std::cout << "h -> hero, t -> tavern, e -> enemy\n";
		std::cout << "w -> up, s -> down, a -> left, d -> rigth\n";
		printMap();
		readMove();
		readEvents();
		if (checkWin())
		{
			break;
		}
		gameLife = hero->isLive();
	}
	if (!gameLife)
	{
		lose();
	}
}

Game::~Game()
{
	finalize();
}
