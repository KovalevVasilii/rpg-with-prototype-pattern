#pragma once
#include "GameObject.h"

class Tavern :
	public GameObject
{
private:
	bool isAlive = true;
public:
	bool isLive() override { return isAlive; }
	void setLiveFalse() override { isAlive = false; }
	Tavern() = default;
	~Tavern() = default;
};

